<?php
class HttpCurl
{

    private $context;
    private $url;

    public function __construct(string $url)
    {
        $this->url = $url;
        $this->context = curl_init($url);
        curl_setopt($this->context, CURLOPT_RETURNTRANSFER, true);
        return $this;
    }

    public function fetchWithPost(array $data)
    {
        curl_setopt($this->context, CURLOPT_POSTFIELDS, $data);
        return curl_exec($this->context);
    }

    public function fetchWithGet(array $data)
    {
        curl_setopt($this->context, CURLOPT_URL, $this->url . "?" . http_build_query($data));
        return curl_exec($this->context);
    }

    public function __destruct()
    {
        curl_close($this->context);
    }
}

