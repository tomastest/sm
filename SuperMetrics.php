<?php
include('HttpCurl.php');

class SuperMetrics
{

    const URL_GET_TOKEN = 'https://api.supermetrics.com/assignment/register';
    const URL_GET_POSTS = 'https://api.supermetrics.com/assignment/posts';
    const TOTAL_PAGES = 10;

    private $curlInitGet;
    private $token;

    public function __construct(array $userData)
    {
        $this->token = $this->requestToken($userData);
        $this->curlInitGet = new HttpCurl(self::URL_GET_POSTS);
        return $this;
    }

    private function requestToken(array $userData) : string
    {
        $response = (new HttpCurl(self::URL_GET_TOKEN))->fetchWithPost($userData);
        $decodedRes = json_decode($response);
        if (empty($decodedRes->data->sl_token)) {
            throw new InvalidArgumentException($response);
        }
        return $decodedRes->data->sl_token;
    }

    public function getPosts(int $page) : array
    {
        $postParams = ['sl_token' => $this->token,'page' => $page];
        $response = $this->curlInitGet->fetchWithGet($postParams);
        $decodedRes = json_decode($response);
        if (empty($decodedRes->data->posts)) {
            throw new InvalidArgumentException($response);
        }
        return $decodedRes->data->posts;
    }

    public function loadAllPosts() : array
    {
        $allPosts = [];
        foreach (range(1, self::TOTAL_PAGES) as $page) {
            $postsPerPage = $this->getPosts($page);
            foreach ($postsPerPage as $post) {
                $post->monthNumber = date("Y-M", strtotime($post->created_time));
                $post->weekNumber = date("Y-M-W", strtotime($post->created_time));
                $post->postLength = strlen($post->message);

                $allPosts[] = $post;
            }
        }
        return $allPosts;
    }


    public static function calcAvgPostLenPerMonth(array $postsArray) : array
    {
        $avgPostLenPerMonth = [];
        $totalCharsPerMonth = [];
        $totalPostsPerMonth = [];

        foreach ($postsArray as $post) {
            if (!isset($totalCharsPerMonth[$post->monthNumber])) {
                $totalCharsPerMonth[$post->monthNumber] = 0;
            }
            if (!isset($totalPostsPerMonth[$post->monthNumber])) {
                $totalPostsPerMonth[$post->monthNumber] = 0;
            }
            if (!isset($avgPostLenPerMonth[$post->monthNumber])) {
                $avgPostLenPerMonth[$post->monthNumber] = 0;
            }

            $totalCharsPerMonth[$post->monthNumber] += $post->postLength;
            $totalPostsPerMonth[$post->monthNumber] += 1;
            $avgPostLenPerMonth[$post->monthNumber] = round($totalCharsPerMonth[$post->monthNumber] / $totalPostsPerMonth[$post->monthNumber]);
        }
        return $avgPostLenPerMonth;
    }

    public static function calcLongestPostPerMonth(array $postsArray) : array
    {
        $longestPostPerMonth = [];
        foreach ($postsArray as $post) {
            if (!isset($longestPostPerMonth[$post->monthNumber])) {
                $longestPostPerMonth[$post->monthNumber] = '';
            }

            if (strlen($longestPostPerMonth[$post->monthNumber]) < $post->postLength) {
                $longestPostPerMonth[$post->monthNumber] = $post->message;
            }
        }
        return $longestPostPerMonth;
    }

    public static function calcTotalPostsSplitByWeek(array $postArray) : array
    {
        $totalPostsByWeek = [];
        foreach ($postArray as $post) {
            $totalPostsByWeek[$post->weekNumber] = isset($totalPostsByWeek[$post->weekNumber]) ?  $totalPostsByWeek[$post->weekNumber] + 1 : 1;
        }
        return $totalPostsByWeek;
    }

    public static function calcTotalPostsPerUserPerMonth(array $postArray) :array
    {
        $totalPostsPerUserPerMonth = [];
        foreach ($postArray as $post) {
            if (isset($totalPostsPerUserPerMonth[$post->monthNumber][$post->from_name])) {
                $totalPostsPerUserPerMonth[$post->monthNumber][$post->from_name] += 1;
            } else {
                $totalPostsPerUserPerMonth[$post->monthNumber][$post->from_name] = 1;
            }
        }
        return $totalPostsPerUserPerMonth;
    }
}
