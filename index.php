<?php

    include("SuperMetrics.php");

    $userData= ['client_id' => 'ju16a6m81mhid5ue1z3v2g0uh', 'email' => 'tomas@gmail.com', 'name' => 'Tomas'];

    $postsArray = (new SuperMetrics($userData))->loadAllPosts();

    $calcData = [
        'Average character length / post / month'   => SuperMetrics::calcAvgPostLenPerMonth($postsArray),
        'Longest post by character length / month'  => SuperMetrics::calcLongestPostPerMonth($postsArray),
        'Total posts split by week'                 => SuperMetrics::calcTotalPostsSplitByWeek($postsArray),
        'Average number of posts per user / month'  => SuperMetrics::calcTotalPostsPerUserPerMonth($postsArray)
    ];


    echo json_encode($calcData);
